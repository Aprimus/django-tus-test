from django.http import HttpResponse
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST


class Index(TemplateView):
    template_name = 'frontend/form.html'


@csrf_exempt
@require_POST
def upload_file(request):
    hook_name = request.META.get('HTTP_HOOK_NAME', '')
    print(hook_name)
    # Add logic for hooks here...
    return HttpResponse(status=200)
