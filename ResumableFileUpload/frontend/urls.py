from django.conf.urls import url
from . import views

app_name = 'frontend'
urlpatterns = [
    url(r'^upload-file/$', views.upload_file, name='upload_file'),
    url(r'', views.Index.as_view(), name='index'),
]